"""Class for  SD21 ServoController. """

#This class was specially created to utilize the functionality of "SD21" card.
#You got to upload the "arduino_sd21.ino" file on the Arduino board. 
#The "arduino_sd21.ino" file was specially created to implement the functions of SD21 on Arduino board.
#The main functions are in the library created for arduino . You can see sd21.h and sd21.cpp files.
#Data sent from BBB to Arduino  are function,Reg,data,servo.
#The SD21 card information was taken from: http://www.robot-electronics.co.uk/htm/sd21tech.htm 

import time
import serial
import Adafruit_BBIO.UART as UART
UART.setup("UART1")                                     

class SD21:

	def __init__(self):
		self.PORT   = '/dev/ttyO1'
		self.SPEED  = 115200            # baudrate -> tested with 115200
		
	def connect(self):
		self.module = serial.Serial(self.PORT, self.SPEED, timeout=1)

	def disconnect(self):
		self.module.close()

	def getBatteryLevel(self):              			#function = 1
		
		self.module.write("1\n")
		self.module.write("0\n")
		self.module.write("0\n")
		self.module.write("0\n")
		time.sleep(0.01)
		batteryLevel = self.module.readline()
		voltage = 0.039*float(batteryLevel)
		print voltage

	def getVersion(self):								#function = 2

		self.module.write("2\n")
		self.module.write("0\n")
		self.module.write("0\n")
		self.module.write("0\n")
		time.sleep(0.01)
		version = self.module.readline()
		print version

	def setPosition(self,servo,angle):    				#function = 3
		"""Set servo position by sending a singleByte"""

		if(angle<0 or angle>180):
			print("Error, Ingrese un valor entre 0 y 180")
		else:
			x = angle*255./180 # converted to a digital value..
        	d = int(x)
            self.module.write("3\n")
			self.module.write("0\n")
			self.module.write(str(d)+"\n")
			self.module.write(str(servo)+"\n")
			           
	def readData(self,Reg):	                         	#function = 4
		""" Read one byte from specified register."""
		_reg = str(Reg)+"\n"	
		self.module.write("4\n")
		self.module.write(str(_reg))
		self.module.write("0\n")
		self.module.write("0\n")		
		time.sleep(0.01)
		_data= self.module.readline()
		print (_data)
	
	def sendData(self,Reg,data):     					#function = 5
	"""Sends data byte to specified register""" 
		self.module.write("5\n")
		self.module.write(str(Reg)+"\n")
		self.module.write(str(data)+ "\n")
		self.module.write("0\n")	
	
	def setSpeed(self, servo=1, speed=0):				#function = 6
	"""Set servo speed. """

		self.module.write("6\n")
		self.module.write("0\n")		
		self.module.write(str(speed)+"\n")
		self.module.write(str(servo)+ "\n")				
	
			
	def setPosOffset(self, servo=1, position=0):		#function = 7

		self.module.write("7\n")
		self.module.write("0\n")		
		self.module.write(str(position)+"\n")
		self.module.write(str(servo)+ "\n")
			
	def setNegOffset(self, servo=1, position=0): 		#function = 8

		self.module.write("8\n")
		self.module.write("0\n")		
		self.module.write(str(position)+"\n")
		self.module.write(str(servo)+ "\n")

	def setPulseWidth(self, servo=1, position=0):		#function = 9
	
		self.module.write("9\n")
		self.module.write("0\n")		
		self.module.write(str(position)+"\n")
		self.module.write(str(servo)+ "\n")

	def getPulseWidth(self, servo=1):                 	#function = 10

		self.module.write("10\n")
		self.module.write("0\n")		
		self.module.write("0\n")
		self.module.write(str(servo)+ "\n")
		servoPulseWidth = self.module.readline()
		print (servoPulseWidth)
			
	

	
