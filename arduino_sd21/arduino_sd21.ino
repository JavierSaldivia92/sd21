#include <Wire.h>
#include "SD21.h"
#include "stdlib.h"

byte address = 0x61; // The address of the SD21

//Data sent from BBB to Arduino  are function,Reg,data,servo

int function,Reg,data,servo;   
char buffer[4];
unsigned char i=0, ok=0;
int input;
int v = 0;

SD21 sd21(address);

void setup() {

Serial.begin(115200);  // Start Serial Communication. BaudRate =115200
Wire.begin();
 
}

void loop() {

if (Serial.available()>0){
   input = Serial.read();
   if(input!='\n'){
      buffer[i] = input;
      buffer[i+1]='\0';
      i++;
     }
   else ok=1; 
   }
  
if(ok==1){
      
   if (v==0){  
      function = atoi(buffer);
       }
      else if (v==1){  
      Reg = atoi(buffer);
      }
      else if (v==2){  
      data = atoi(buffer);
      } 
      else {  
      servo = atoi(buffer);
      } 
      v = v+1; 
      if (v==4){

  switch(function){

  case 0:
  break;
  
  case 1:
  
  sd21.getBatteryLevel();
  function=0;
  break;

  case 2:
  
  sd21.getVersion();
  function=0;
  break;

  case 3:
  
  sd21.set_position(servo,data);
  function=0;
  break;

  case 4:

  sd21.read_data(Reg);
  function=0;
  break;
    
  case 5:

  sd21.send_data(Reg,data);
  function=0; 
  break;   

  case 6:

  sd21.setServoSpeed(servo,data);
  function = 0;
  break;

  case 7:

  sd21.setServoPosOffset(servo,data);
  function=0;
  break;

  case 8:
  
  sd21.setServoNegOffset(servo,data);
  function=0;
  break;

  case 9:

  sd21.setServoPulseWidth(servo,data);
  function = 0;
  break;

  case 10:
  sd21.getServoPulseWidth(servo);
  function = 0;
  break;
    
  } 
     v=0;
        }
      ok=0;  
      i=0;    
  } 



}

  

