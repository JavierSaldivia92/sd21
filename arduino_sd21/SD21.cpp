#include "SD21.h"
#include <Wire.h>

SD21::SD21(byte address){
	_address=address;
	Wire.begin();
	Serial.begin(115200);
}

int SD21::servoBaseReg(byte servo){   
 /*Function to calculate the specified servo's base register*/

  if(servo<1 || servo>21){
    
    servo=1;

  }
  return 3*(servo -1);
}

int SD21::splitDoubleByte(int dbyte){
  // Split a double byte value in single bytes list
  
    loByte = dbyte & 0xFF;
    hiByte = (dbyte & 0xFF00) >> 8;
    return (loByte, hiByte);
  }  

void SD21::getBatteryLevel(){
 /*Get the servo battery voltage in 39mV units up to a maximum of 10v. 
  For example, a battery voltage of 7.2v will read about 184.*/

	_numBytes=1;
  batteryReg=65;
 
	Wire.beginTransmission(_address);
	Wire.write(batteryReg);
	Wire.endTransmission();
	Wire.requestFrom(_address,_numBytes); 
  battery_level=Wire.read();
  delayMicroseconds(10000);
  Serial.println(battery_level);
  
  }

void SD21::getVersion(){
  /* Get software version.*/
 
  _numBytes=1;
  versionReg=64;
  Wire.beginTransmission(_address);
  Wire.write(versionReg);
  Wire.endTransmission();
  Wire.requestFrom(_address,_numBytes); 
  int Version=Wire.read();
  delayMicroseconds(10000);
  Serial.println(Version);  
  
  }

void SD21::set_position(int servo,byte Position){
    /* Set servo position by sending single byte. */

    baseReg=63;
    servoReg=baseReg-1 + servo;
    Wire.beginTransmission(_address);
    Wire.write(servoReg);
    Wire.write(Position);
    Wire.endTransmission();
     }

  
int SD21::read_data(byte Reg){
  /* Read one byte from specified register */
  
  _numBytes=1;  
  Wire.beginTransmission(_address);
  Wire.write(Reg);
  Wire.endTransmission();
  Wire.requestFrom(_address,_numBytes); 
  byte dataRead=Wire.read();
  delayMicroseconds(10000);
  Serial.println(dataRead);
  return dataRead;  
  }  


void SD21::send_data(byte Reg,byte data){
/* Sends data byte to specified register. */
  
  Wire.beginTransmission(_address);
  Wire.write(Reg);
  Wire.write(data);
  Wire.endTransmission();
  }
 
void SD21::setServoSpeed(int servo,byte speed_servo){
  /* Set servo speed. */
    
  baseReg = servoBaseReg(servo);
  Wire.beginTransmission(_address);
  Wire.write(baseReg);
  Wire.write(speed_servo);
  Wire.endTransmission();
  
  }

void SD21::setServoPosOffset(int servo,byte Position){
     /*Set positive position offset (see setServoPosition). 

        Parameters
        ----------
        servo : int
            
        position : int (byte)
            Positive offset: 0 to 255 (Defaults to 0)*/
    
    baseReg  = 84;
    servoReg = baseReg - 1 + servo;
    Wire.beginTransmission(_address);
    Wire.write(servoReg);
    Wire.write(Position);
    Wire.endTransmission();
  
  }

void SD21::setServoNegOffset(int servo,byte Position){
  /* Set negative position offset (see setServoPosition). 

        Parameters
        ----------
        servo : int
            Servo number (1 .. 21).
        position : int (byte)
            Negative offset: 0 to 255 (Defaults to 0). */

  baseReg  = 105;
  servoReg = baseReg - 1 + servo;
  Wire.beginTransmission(_address);
  Wire.write(servoReg);
  Wire.write(Position);
  Wire.endTransmission();

  }

void SD21::setServoPulseWidth(int servo,byte pulseWidth){
  /* Set servo position by sending pulse width in us. */

  baseReg = servoBaseReg(servo);
  loReg   = baseReg + 1;
  hiReg   = baseReg + 2;
  loByte,hiByte = splitDoubleByte(pulseWidth);
  send_data(loReg,loByte);
  send_data(hiReg,hiByte);
  }

int SD21::getServoPulseWidth(int servo){
  /* Get pulse width during speed controlled movement. */

  baseReg= servoBaseReg(servo);
  loReg = baseReg + 1;
  hiReg = baseReg + 2;
  loByte = read_data(loReg); 
  hiByte = read_data(hiReg);
  pulseWidth = (hiByte<<8)|loByte;
  Serial.println(pulseWidth);
  return pulseWidth;
 }  


  
  
