//Class for SD21 ServoController
//The SD21 card information was taken from: http://www.robot-electronics.co.uk/htm/sd21tech.htm 
#include "Arduino.h"
#include "Wire.h"

class SD21 {

	public:
        
	SD21(uint8_t address);
    int splitDoubleByte(int dbyte);
    int servoBaseReg(byte servo);
    void send_data(byte Reg,byte data);
	  int read_data(byte Reg);
    void setServoPulseWidth(int servo,byte pulseWidth);
    void setServoSpeed(int servo,byte speed_servo);
    void set_position(int servo,byte Position);
    void setServoPosOffset(int servo,byte Position);
    void setServoNegOffset(int servo,byte Position);
    int getServoPulseWidth(int servo);
    void getBatteryLevel();
    void getVersion();
   
           
	private:
	
	byte loReg;
    byte hiReg;
    int loByte;
    int hiByte;
    int pulseWidth;
  	uint8_t _address;
  	uint8_t _numBytes;
	  byte _data;
    int battery_level;
    byte versionReg;
    byte batteryReg;
    int baseReg;   
    int servoReg;      

};
